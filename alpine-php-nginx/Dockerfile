FROM php:7.3-fpm-alpine3.14

LABEL maintainer="Jonatas Walker <jonataswalker@gmail.com>"

ENV NGINX_VERSION 1.21.1
ENV NGINX_NJS_VERSION   0.6.1
ENV NGINX_PKG_RELEASE   1
ENV NGINX_CONF_PREFIX=/etc/nginx \
  PHP_CONF_PREFIX=/usr/local/etc/php/conf.d \
  PHP_FPM_CONF_PREFIX=/usr/local/etc/php-fpm.d

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

RUN set -x \
    && echo http://alpinelinux.c3sl.ufpr.br/v3.14/community/ >> /etc/apk/repositories \
    && apk update \
    && apk upgrade --no-cache \
    && apk add --no-cache \
        tzdata \
        supervisor \
        git \
        gnu-libiconv \
        icu-dev \
        libmcrypt-dev \
        libzip-dev \
        zip \
        unzip \
        htop \
  && apk add --no-cache --virtual .build-deps \
        alpine-sdk \
        automake  \
        autoconf  \
        binutils  \
        binutils-gold  \
        build-base \
        bzip2-dev \
        findutils \
        freetype-dev \
        geoip-dev \
        gd-dev \
        gcc  \
        g++  \
        gnupg1  \
        gzip \
        libgcc  \
        libtool  \
        libc-dev \
        libedit-dev \
        libressl-dev \
        libxslt-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libmemcached-dev \
        libxml2-dev \
        libzip-dev \
        linux-headers \
        make \
        mercurial \
        openssl-dev \
        perl-dev \
        pcre-dev \
        zlib-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j "$(nproc)" \
        gd \
        iconv \
        intl \
        opcache \
        mysqli \
        pdo_mysql \
        soap \
        zip \
    # && pecl install memcached-3.1.5  \
    && pecl install redis-5.3.4  \
    && pecl install mcrypt-1.0.4  \
    # && pecl install imagick-3.5.1 \
    && docker-php-ext-enable redis mcrypt \
    && apkArch="$(cat /etc/apk/arch)" \
    && nginxPackages=" \
        nginx=${NGINX_VERSION}-r${NGINX_PKG_RELEASE} \
        nginx-module-xslt=${NGINX_VERSION}-r${NGINX_PKG_RELEASE} \
        nginx-module-geoip=${NGINX_VERSION}-r${NGINX_PKG_RELEASE} \
        nginx-module-image-filter=${NGINX_VERSION}-r${NGINX_PKG_RELEASE} \
        nginx-module-njs=${NGINX_VERSION}.${NGINX_NJS_VERSION}-r${NGINX_PKG_RELEASE} \
    " \
    && case "$apkArch" in \
        x86_64|aarch64) \
# arches officially built by upstream
            set -x \
            && KEY_SHA512="e7fa8303923d9b95db37a77ad46c68fd4755ff935d0a534d26eba83de193c76166c68bfe7f65471bf8881004ef4aa6df3e34689c305662750c0172fca5d8552a *stdin" \
            && apk add --no-cache --virtual .cert-deps \
                openssl \
            && wget -O /tmp/nginx_signing.rsa.pub https://nginx.org/keys/nginx_signing.rsa.pub \
            && if [ "$(openssl rsa -pubin -in /tmp/nginx_signing.rsa.pub -text -noout | openssl sha512 -r)" = "$KEY_SHA512" ]; then \
                echo "key verification succeeded!"; \
                mv /tmp/nginx_signing.rsa.pub /etc/apk/keys/; \
            else \
                echo "key verification failed!"; \
                exit 1; \
            fi \
            && apk del .cert-deps \
            && apk add -X "https://nginx.org/packages/mainline/alpine/v$(egrep -o '^[0-9]+\.[0-9]+' /etc/alpine-release)/main" --no-cache $nginxPackages \
            ;; \
        *) \
# we're on an architecture upstream doesn't officially build for
# let's build binaries from the published packaging sources
            set -x \
            && tempDir="$(mktemp -d)" \
            && chown nobody:nobody $tempDir \
            && su nobody -s /bin/sh -c " \
                export HOME=${tempDir} \
                && cd ${tempDir} \
                && hg clone https://hg.nginx.org/pkg-oss \
                && cd pkg-oss \
                && hg up ${NGINX_VERSION}-${NGINX_PKG_RELEASE} \
                && cd alpine \
                && make all \
                && apk index -o ${tempDir}/packages/alpine/${apkArch}/APKINDEX.tar.gz ${tempDir}/packages/alpine/${apkArch}/*.apk \
                && abuild-sign -k ${tempDir}/.abuild/abuild-key.rsa ${tempDir}/packages/alpine/${apkArch}/APKINDEX.tar.gz \
                " \
            && cp ${tempDir}/.abuild/abuild-key.rsa.pub /etc/apk/keys/ \
            && apk add -X ${tempDir}/packages/alpine/ --no-cache $nginxPackages \
            ;; \
    esac \
# if we have leftovers from building, let's purge them (including extra, unnecessary build deps)
    && if [ -n "$tempDir" ]; then rm -rf "$tempDir"; fi \
    && if [ -n "/etc/apk/keys/abuild-key.rsa.pub" ]; then rm -f /etc/apk/keys/abuild-key.rsa.pub; fi \
    && if [ -n "/etc/apk/keys/nginx_signing.rsa.pub" ]; then rm -f /etc/apk/keys/nginx_signing.rsa.pub; fi \
# Bring in gettext so we can get `envsubst`, then throw
# the rest away. To do this, we need to install `gettext`
# then move `envsubst` out of the way so `gettext` can
# be deleted completely, then move `envsubst` back.
    && apk add --no-cache --virtual .gettext gettext \
    && mv /usr/bin/envsubst /tmp/ \
    \
    && runDeps="$( \
        scanelf --needed --nobanner /tmp/envsubst \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | sort -u \
            | xargs -r apk info --installed \
            | sort -u \
    )" \
    && apk add --no-cache $runDeps \
    && apk del .gettext \
    && mv /tmp/envsubst /usr/local/bin/ \
# forward request and error logs to docker log collector
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && apk del .build-deps \
    && rm -f ${NGINX_CONF_PREFIX}/nginx.conf ${PHP_FPM_CONF_PREFIX}/*

COPY conf/limits.conf /etc/security/limits.conf
COPY conf/nginx/ ${NGINX_CONF_PREFIX}/
COPY conf/php/docker.ini conf/php/opcache.ini ${PHP_CONF_PREFIX}/
COPY conf/php/fpm/ ${PHP_FPM_CONF_PREFIX}/
COPY conf/supervisor.conf /etc/supervisor/supervisor.conf
COPY docker-entrypoint.sh /usr/local/bin
# COPY --chown=www-data:www-data info.php /var/www/html

EXPOSE 80

STOPSIGNAL SIGQUIT

# be sure nginx is properly passing to php-fpm and fpm is responding
HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl -f http://localhost:8181/ping || exit 1

# Let supervisord start nginx & php-fpm
ENTRYPOINT ["docker-entrypoint.sh"]
