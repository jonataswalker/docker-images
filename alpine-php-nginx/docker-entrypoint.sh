#!/bin/sh

set -e

/usr/bin/supervisord -c /etc/supervisor/supervisor.conf

exec "$@"